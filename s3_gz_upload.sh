#!/bin/bash
# Nrgene pre-processing S3 data uploader by: Paul@nrgene.com

set -e
set -u


# default config - you probably do not need to change anything below this line
export AWS_DEFAULT_REGION='us-east-1'
EXEC_DIR=$(dirname `readlink -f $0`)
MD5_CHECKSUM=0
FORCE_OVERRIDE=0
ENABLE_READ_ALL=0
RUN_ID=$(head -3 /dev/urandom|tr -cd '0-9'|cut -c 1-5)
AES_KEY_FILE="aes_tmp.key"
S3_LIST="s3_list.txt"
AES_ENC_KEY=$(openssl rand -base64 32)
AWS_SNS_ARN=${AWS_SNS_ARN:-arn:aws:sns:us-east-1:759967409705:nrgene-illinois-upload}
ENCRYPTION=1
# functions

usage() {
cat << EOF 

=== Nrgene S3 data uploader ===

The script iterates over a given list of files (found recursively under '-p /path/to/files') archives , encrpyts each file individually and uploads them to S3.

Usage:
------
$0 -p /path/to/files -n <project-name>  -b <bucket_name> [-s toggle md5 checksum , off by default] [-a enable read by everyone] [-f force_override][ -d disable_encryption ]

Example:
--------
$0 -p /path/to/files -n ProjectX -b mybucket -s -a -f

EOF
exit 0
}


log() {
echo -e `date +" [%Y-%m-%d %H:%M:%S]   "`"$1"
}

file_uniq_verify() {
CHECK_FILES_PATH=$1

DUPFILES=$(find  ${CHECK_FILES_PATH} -type f |xargs -i -n1 basename {}|sort|uniq -c|grep -v '1\ .*'|wc -l)

if [[ ${DUPFILES} > 0  ]];then
  echo "duplicates names found, please make sure all the files are named uniquely"
  exit 1
fi

}

s3_verify() {
TEST_FILE='/tmp/s3_gz_upload_test'
touch ${TEST_FILE}
aws s3 cp ${TEST_FILE} s3://${S3_BUCKET}/${TEST_FILE} && aws s3 rm s3://${S3_BUCKET}/${TEST_FILE}  || exit 1
}

names_verify() {
CHECK_BUCKET_NAME=$1
CHECK_PROJ_NAME=$2

if [[ ${CHECK_BUCKET_NAME} =~ ^s3\:  ]] ;then

  echo "bucket name should not start with s3"
  exit 1

elif [[ ${CHECK_PROJ_NAME} == *\/* ]] ;then

  echo "project name should not contain slashes (/)"
  exit 1

elif [[ ${CHECK_BUCKET_NAME} == *\/* ]] ;then

  echo "bucket name should not contain slashes (/)"
  exit 1

fi
}

zip_data () {
FILE_NAME=$1

pigz -f -k -4 -b 32 ${FILE_NAME} || exit 1
}


encrypt_data() {
FILE_NAME=$1

echo "${AES_ENC_KEY}" | openssl enc -aes-256-cbc -salt -in ${FILE_NAME}.gz -out ${FILE_NAME}.gz.enc -pass stdin || exit 1
}

s3_upload() {
RETRY_NUM=5
RETRY=0
SUCCESS=0
FILE_NAME=$1
MD5_CHECKSUM=$2

# if encrpytion is enabled, set an appropriate suffix
if [[ ${ENCRYPTION} == 1 ]];then
  SUFFIX="gz.enc"
else
  SUFFIX="gz"
fi

# if enabled, calculate md5 check-sum for data integrity verification
if [[ ${MD5_CHECKSUM} == 1 ]]; then 
  MD5_CHECKSUM=$(md5sum ${FILE_NAME}.${SUFFIX}|awk '{print $1}')
else
  MD5_CHECKSUM='N/A'
fi

# unless force override is enabled, check if detination file already exists
if [[ ${FORCE_OVERRIDE} == 0 ]]; then
  if $(aws s3 ls s3://${S3_BUCKET}/${PROJ_NAME}/${FILE_NAME}.gz.enc &>/dev/null) ;then
    echo "Files: ${FILE_NAME}.${SUFFIX} and s3://${S3_BUCKET}/${PROJ_NAME}/${FILE_NAME}.${SUFFIX} have the same basename,  please make sure the files are named uniquely (use -f to force over-ride)"
    exit 1
  fi
fi

while [[ ${RETRY} < ${RETRY_NUM} ]] ;do
  AWS_GRANTS=""
  if [[ ${ENABLE_READ_ALL} == 1 ]];then
    AWS_GRANTS="--grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers"
  fi
  
   aws s3 cp ${FILE_NAME}.${SUFFIX} s3://${S3_BUCKET}/${PROJ_NAME}/${FILE_NAME}.${SUFFIX} ${AWS_GRANTS} && SUCCESS=1 && \
   echo -e "${PROJ_NAME},${S3_BUCKET},${PROJ_NAME}/${FILE_NAME}.${SUFFIX},${MD5_CHECKSUM},${AES_ENC_KEY}" >> /tmp/${S3_LIST}_${RUN_ID}

  if [[ ${SUCCESS} == 1 ]] ;then
     break
  fi
done
}

cleanup() {
PATH_TO_DEL=$1

\rm -fr ${PATH_TO_DEL}.gz* 

}

sns_notify() {
MSG=`cat /tmp/${S3_LIST}_${RUN_ID}`
echo -e "\n sending upload details via sns notification..."
aws sns publish --topic-arn ${AWS_SNS_ARN} --message "${MSG}" 
}

##############################################
# Main
###############################################

# get external arguments + more sanity check
while getopts "p:n:b:ahsfd" flag ; do
  case $flag in
    p ) FILE_LIST_PATH=${OPTARG};;
    n ) PROJ_NAME=${OPTARG};;
    s ) MD5_CHECKSUM=1;;
    d ) ENCRYPTION=0;;
    b ) S3_BUCKET=${OPTARG};;
    a ) ENABLE_READ_ALL=1;;
    f ) FORCE_OVERRIDE=1;;
    * ) usage;;
    esac
done


# perform naming verification
names_verify ${S3_BUCKET} ${PROJ_NAME}

# perform a test write to destiantion bucket
s3_verify

# perform file uniquenes verification
file_uniq_verify ${FILE_LIST_PATH}

# fetch files, ignore already encrypted or gzipped files
echo "==== `date` starting run, results will be saved to: /tmp/${S3_LIST}_${RUN_ID} ==="
FILE_LIST=`find ${FILE_LIST_PATH} -type f ! \( -name "*enc" -o -name "*gz" \)`


# iterate over list of files , gzip every single file, encrypt it and upload to S3 flattened
for FILE in ${FILE_LIST}; do
  FILE_DIR=$(dirname ${FILE})
  FILE_NAME=$(echo $FILE|awk -F"/" '{print $NF}')
  cd ${FILE_DIR} 
  log "[info] zipping file ${FILE_NAME}"; zip_data "${FILE_NAME}" && \
  if [[ ${ENCRYPTION} == 1 ]] ;then
    log "[info] encrypting file ${FILE_NAME}.gz" ; encrypt_data "${FILE_NAME}"
  fi
  if [[ ${MD5_CHECKSUM} == 1 ]]; then
    log "[info] starting upload to s3"; s3_upload "${FILE_NAME}" 1 
  else
    log "[info] starting upload to s3"; s3_upload "${FILE_NAME}" 0 
  fi && \
  log "[info] cleaning up" ; cleanup ${FILE_NAME} && \
  cd - &>/dev/null
done && \

sns_notify && \rm -rf /tmp/${S3_LIST}_${RUN_ID} || echo -e "\nsns send failed, upload details stored at /tmp/${S3_LIST}_${RUN_ID}"
