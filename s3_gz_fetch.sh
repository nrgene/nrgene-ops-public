#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset


AES_ENC_KEY=""
AWS_BIN=`which aws`
PIGZ_BIN=`which pigz`
OPENSSL_BIN=`which openssl`

# Functions
usage() {
cat << EOF 
Usage:
------
$0 -s s3://bucket-name/path -d /local/path/to/extract/to -k AES_ENC_KEY

EOF
exit 0
}

log() {
echo -e `date +" [%Y-%m-%d %H:%M:%S]   "`"$1"
}


# Get flags
while getopts "s:d:k:" flag ; do
  case $flag in
    s ) SOURCE_BUCKET=${OPTARG};;
    d ) DESTINATION_DIR=${OPTARG};;
    k ) AES_ENC_KEY=${OPTARG};;
    * ) usage;;
  esac
done


# system pre-flight sanity check
if [ ! -e ${AWS_BIN} ];then
    echo "[error] 'aws cli' not found (pip install awscli)"
    exit 1
fi

if [ ! -e ${OPENSSL_BIN} ];then
    echo "[error] 'openssl' not found"
    exit 1
fi

if [ ! -e ${PIGZ_BIN} ];then
  echo "[error] 'pigz' not found"
  exit 1
fi

if [ ! -e ${PIGZ_BIN} ];then
  echo "[error] 'pigz' not found"
  exit 1
fi

if [ -z ${AWS_ACCESS_KEY_ID} ] || [ -z ${AWS_SECRET_ACCESS_KEY} ]; then
  echo "[error] aws credentials (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY ) are not set"
  exit 1
fi

if [ ! -d ${DESTINATION_DIR} ] ;then
  mkdir -p ${DESTINATION_DIR} || echo "cannot create dst dir: ${DESTINATION_DIR}" ; exit 1
fi

#Fetch S3 data, decrypt and untar contents of synced folder
aws s3 sync ${SOURCE_BUCKET} ${DESTINATION_DIR} && \
ENC_FILES=$(find ${DESTINATION_DIR} -type f -name "*.enc")
for ENC_FILE in ${ENC_FILES} ;do
  log "Decrypting => ${ENC_FILE}"
  OUTPUT_FILE=$(echo ${ENC_FILE} | sed -e 's/.enc//')
  ${OPENSSL_BIN} enc -d -aes-256-cbc -salt -in ${ENC_FILE} -out ${OUTPUT_FILE} -pass pass:${AES_ENC_KEY} && \
  ${PIGZ_BIN} -d ${OUTPUT_FILE} && \
  \rm -fr ${ENC_FILE}
done
